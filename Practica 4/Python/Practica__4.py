import csv
import time
import os


class Archive:
    def __init__(self, filename):
        self.data = []
        self.filename = filename
        self.read()

    def read(self):
        try:
            open(self.filename)
        except IOError:
            print("No se pudo abrir el archivo " + self.filename)
            exit(1)
        else:
            with open("prac1-valores.csv") as database:
                line = csv.reader(database, delimiter=',')
                for row in line:
                    self.data.append(row)
            self.data = [item for sublist in self.data for item in sublist]
            self.data = self.data[10:self.data.__sizeof__()]
            self.data = list(map(int, self.data))


class Menu:
    def __init__(self):
        self.menu_op = 0

    def show_menu(self):
        os.system('cls')
        print("Logistic Beach")
        print("Software de Evaluacion")
        print("1.-Salir")
        print("2.-Guardar Resultados en un archivo")
        print("3.-Aplicar Ordenamiento \"Bubble Sort\"")
        print("4.-Aplicar Ordenamiento \"Insertion Sort\"")
        print("5.-Aplicar Ordenamiento \"Shell Sort\"")
        print("6.-100 Numeros de error mas frecuentes")
        print("7.-100 Numeros de error menos frecuentes")
        print("8.-Mostrar el numero promedio de error")
        print("9.-Numero de veces que se presento cada fase")
        print("10.-Fase con mayor presentacion en el proceso")

    def selection(self):
        self.menu_op = int(input("Opcion: "))
        os.system('cls')
        if self.menu_op == 2:
            sav = SaveMenu()
            while sav.menu_op != 0:
                sav.show_menu()
                sav.selection()
        elif self.menu_op == 3:
            bubble_sort()
        elif self.menu_op == 4:
            insertion_sort()
        elif self.menu_op == 5:
            shell_sort()
        elif self.menu_op == 6:
            most_frequent()
        elif self.menu_op == 7:
            less_frequent()
        elif self.menu_op == 8:
            avrg_error()
        elif self.menu_op == 9:
            avrg_phase()
        elif self.menu_op == 10:
            most_phase()
        else:
            self.menu = 0


class SaveMenu:
    def __init__(self):
        self.menu_op = 1
        self.filename = ""
        self.askfile()

    def askfile(self):
        self.filename = input("Nombre del archivo: ")
        if self.filename.find('.') == -1:
            self.filename = self.filename + ".csv"
        if(self.filename.find(".csv") == -1):
            self.filename = self.filename + ".csv"

    def show_menu(self):
        os.system('cls')
        print("Logistic Beach")
        print("¿Que desea guardar?")
        print("1.-Ordenamiento \"Bubble Sort\"")
        print("2.-Ordenamiento \"Insertion Sort\"")
        print("3.-Ordenamiento \"Shell Sort\"")
        print("4.-100 numeros de error mas frecuentes")
        print("5.-100 Numeros de error menos frecuentes")
        print("6.-Numero promedio de error")
        print("7.-Numero de veces que se presento cada fase")
        print("8.-Fase con mayor presentacion en el proceso")

    def selection(self):
        self.menu_op = int(input("Opcion: "))
        os.system('cls')
        if self.menu_op == 1:
            bubble_sortS(self.filename)
        elif self.menu_op == 2:
            insertion_sortS(self.filename)
        elif self.menu_op == 3:
            shell_sortS(self.filename)
        elif self.menu_op == 4:
            most_frequentS(self.filename)
        elif self.menu_op == 5:
            less_frequentS(self.filename)
        elif self.menu_op == 6:
            avrg_errorS(self.filename)
        elif self.menu_op == 7:
            avrg_phaseS(self.filename)
        elif self.menu_op == 8:
            most_phaseS(self.filename)
        else:
            self.menu = 0

# Sorting Methods
def bubble_sort():
    temp_data = database.data.copy()
    n = len(temp_data)
    t = time.process_time()
    for x in range(n - 1):
        for y in range(0, n - x - 1):
            if temp_data[y] > temp_data[y + 1]:
                temp = temp_data[y]
                temp_data[y] = temp_data[y + 1]
                temp_data[y + 1] = temp

    print("Los datos han sido ordenados")
    print("El algoritmo de ordenamiento \"Shell Sort\" ha tardado: ")
    print(str(time.process_time() - t) + " segundos en ordenar 100000 datos")
    print("¿Mostrar datos?")
    print("1.-Si")
    print("2.-No")
    if int(input("Opcion: ")) == 1:
        print(temp_data)
        input("Presione enter para continuar")


def bubble_sortS(filename):
    temp_data = database.data.copy()
    n = len(temp_data)
    for x in range(n - 1):
        for y in range(0, n - x - 1):
            if temp_data[y] > temp_data[y + 1]:
                temp = temp_data[y]
                temp_data[y] = temp_data[y + 1]
                temp_data[y + 1] = temp
    file = open(filename, mode='w+')
    file.write("Numero,Dato\n")
    for x in range(len(temp_data)):
        file.write(str(x + 1) + "," + str(temp_data[x]) + '\n')
    file.close()


def insertion_sort():
    temp_data = database.data.copy()
    t = time.process_time()
    for x in range(1, len(temp_data)):
        y = x
        while (temp_data[y] < temp_data[y - 1]) and int(y) >= 1:
            temp = temp_data[y]
            temp_data[y] = temp_data[y - 1]
            temp_data[y - 1] = temp
            y -= 1
    print("Los datos han sido ordenados")
    print("El algoritmo de ordenamiento \"Shell Sort\" ha tardado: ")
    print(str(time.process_time() - t) + " segundos en ordenar 100000 datos")
    print("¿Mostrar datos?")
    print("1.-Si")
    print("2.-No")
    if int(input("Opcion: ")) == 1:
        print(temp_data)
        input("Presione enter para continuar")


def insertion_sortS(filename):
    temp_data = database.data.copy()
    for x in range(1, len(temp_data)):
        y = x
        while (temp_data[y] < temp_data[y - 1]) and int(y) >= 1:
            temp = temp_data[y]
            temp_data[y] = temp_data[y - 1]
            temp_data[y - 1] = temp
            y -= 1
    file = open(filename, mode='w+')
    file.write("Numero,Dato\n")
    for x in range(len(temp_data)):
        file.write(str(x + 1) + "," + str(temp_data[x]) + '\n')
    file.close()


def shell_sort():
    temp_data = database.data.copy()
    t = time.process_time()
    n = len(temp_data) // 2
    while n:
        for i, el in enumerate(temp_data):
            while i >= n and temp_data[i - n] > el:
                temp_data[i] = temp_data[i - n]
                i -= n
            temp_data[i] = el
        n = 1 if n == 2 else int(n * 5.0 / 11)

    print("Los datos han sido ordenados")
    print("El algoritmo de ordenamiento \"Shell Sort\" ha tardado: ")
    print(str(time.process_time() - t) + " segundos en ordenar 100000 datos")
    print("¿Mostrar datos?")
    print("1.-Si")
    print("2.-No")
    if int(input("Opcion: ")) == 1:
        print(temp_data)
        input("Presione enter para continuar")


def shell_sortS(filename):
    temp_data = database.data.copy()
    n = len(temp_data) // 2
    while n:
        for i, el in enumerate(temp_data):
            while i >= n and temp_data[i - n] > el:
                temp_data[i] = temp_data[i - n]
                i -= n
            temp_data[i] = el
        n = 1 if n == 2 else int(n * 5.0 / 11)
    file = open(filename, mode = 'w+')
    file.write("Numero,Dato\n")
    for x in range(len(temp_data)):
        file.write(str(x+1) + "," + str(temp_data[x])+'\n')
    file.close()


# Frequency Methods
def most_frequent():
    temp_data = database.data.copy()
    n = len(temp_data) // 2
    while n:
        for i, el in enumerate(temp_data):
            while i >= n and temp_data[i - n] > el:
                temp_data[i] = temp_data[i - n]
                i -= n
            temp_data[i] = el
        n = 1 if n == 2 else int(n * 5.0 / 11)
    newlist = []
    number = 1
    counter = 0

    for x in range(0, len(temp_data)):
        if temp_data[x] == number:
            counter += 1
        else:
            newlist.append([number, counter])
            number += 1
            counter = 1
    newlist.append([number, counter])

    for x in range(0, 1000):
        y = x
        while newlist[y][1] < newlist[y - 1][1] and y >= 1:
            temp = newlist[y][1]
            newlist[y][1] = newlist[y - 1][1]
            newlist[y - 1][1] = temp
            temp = newlist[y][0]
            newlist[y][0] = newlist[y - 1][0]
            newlist[y - 1][0] = temp
            y -= 1

    print("Numero\tFrecuencia")
    x = 999
    while x > 899:
        print(str(newlist[x][0]) + "\t\t" + str(newlist[x][1]))
        x -= 1
    input("Presione enter para continuar")


def most_frequentS(filename):
    temp_data = database.data.copy()
    n = len(temp_data) // 2
    while n:
        for i, el in enumerate(temp_data):
            while i >= n and temp_data[i - n] > el:
                temp_data[i] = temp_data[i - n]
                i -= n
            temp_data[i] = el
        n = 1 if n == 2 else int(n * 5.0 / 11)
    newlist = []
    number = 1
    counter = 0

    for x in range(0, len(temp_data)):
        if temp_data[x] == number:
            counter += 1
        else:
            newlist.append([number, counter])
            number += 1
            counter = 1
    newlist.append([number, counter])

    for x in range(0, 1000):
        y = x
        while newlist[y][1] < newlist[y - 1][1] and y >= 1:
            temp = newlist[y][1]
            newlist[y][1] = newlist[y - 1][1]
            newlist[y - 1][1] = temp
            temp = newlist[y][0]
            newlist[y][0] = newlist[y - 1][0]
            newlist[y - 1][0] = temp
            y -= 1
    file = open(filename, mode='w+')
    file.write("Numero,Frecuencia\n")
    x = 999
    while x > 899:
        file.write(str(newlist[x][0]) + "," + str(newlist[x][1])+'\n')
        x -= 1
    file.close()


def less_frequent():
    temp_data = database.data.copy()
    n = len(temp_data) // 2
    while n:
        for i, el in enumerate(temp_data):
            while i >= n and temp_data[i - n] > el:
                temp_data[i] = temp_data[i - n]
                i -= n
            temp_data[i] = el
        n = 1 if n == 2 else int(n * 5.0 / 11)
    newlist = []
    number = 1
    counter = 0

    for x in range(0, len(temp_data)):
        if temp_data[x] == number:
            counter += 1
        else:
            newlist.append([number, counter])
            number += 1
            counter = 1
    newlist.append([number, counter])

    for x in range(0, 1000):
        y = x
        while newlist[y][1] < newlist[y - 1][1] and y >= 1:
            temp = newlist[y][1]
            newlist[y][1] = newlist[y - 1][1]
            newlist[y - 1][1] = temp
            temp = newlist[y][0]
            newlist[y][0] = newlist[y - 1][0]
            newlist[y - 1][0] = temp
            y -= 1

    print("Numero\tFrecuencia")
    x = 0
    while x < 100:
        print(str(newlist[x][0]) + "\t\t" + str(newlist[x][1]))
        x += 1
    input("Presione enter para continuar")


def less_frequentS(filename):
    temp_data = database.data.copy()
    n = len(temp_data) // 2
    while n:
        for i, el in enumerate(temp_data):
            while i >= n and temp_data[i - n] > el:
                temp_data[i] = temp_data[i - n]
                i -= n
            temp_data[i] = el
        n = 1 if n == 2 else int(n * 5.0 / 11)
    newlist = []
    number = 1
    counter = 0

    for x in range(0, len(temp_data)):
        if temp_data[x] == number:
            counter += 1
        else:
            newlist.append([number, counter])
            number += 1
            counter = 1
    newlist.append([number, counter])

    for x in range(0, 1000):
        y = x
        while newlist[y][1] < newlist[y - 1][1] and y >= 1:
            temp = newlist[y][1]
            newlist[y][1] = newlist[y - 1][1]
            newlist[y - 1][1] = temp
            temp = newlist[y][0]
            newlist[y][0] = newlist[y - 1][0]
            newlist[y - 1][0] = temp
            y -= 1
    file = open(filename, mode='w+')
    file.write("Numero,Frecuencia\n")
    x = 0
    while x < 100:
        file.write(str(newlist[x][0]) + "," + str(newlist[x][1])+'\n')
        x += 1
    file.close()


#Averange Methods
def avrg_error():
    temp_data = database.data.copy()
    counter = 0
    for x in range(len(temp_data)):
        counter += temp_data[x]
    counter = counter/len(temp_data)
    print("El numero promedio de error es: "+str(int(counter)))
    counter = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    for x in range(0, len(temp_data), 10):
        for y in range(10):
            counter[y] += temp_data[x+y]
    for y in range(10):
        counter[y] = counter[y]/10000
    print("Promedio de error por fase: ")
    print("F1\tF2\tF3\tF4\tF5\tF6\tF7\tF8\tF9\tF10")
    for y in range(10):
        print(str(int(counter[y])),end = '\t')
    input("\nPresione enter para continuar")


def avrg_errorS(filename):
    temp_data = database.data.copy()
    counter = 0
    for x in range(len(temp_data)):
        counter += temp_data[x]
    counter = counter/len(temp_data)
    file = open(filename, 'w+')
    file.write("El numero promedio de error es: "+str(int(counter))+'\n')
    counter = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    for x in range(0, len(temp_data), 10):
        for y in range(10):
            counter[y] += temp_data[x+y]
    for y in range(10):
        counter[y] = counter[y]/10000

    file.write("Promedio de error por fase: \n")
    file.write("F1,F2,F3,F4,F5,F6,F7,F8,F9,F10\n")
    file.write(str(int(counter[0])) + ',' +str(int(counter[1])) + ',' +str(int(counter[2])) + ',' +str(int(counter[3])) + ',' +str(int(counter[4])) + ',' +str(int(counter[5])) + ',' +str(int(counter[6])) + ',' +str(int(counter[7])) + ',' +str(int(counter[8])) + ',' +str(int(counter[9]))+'\n')
    file.close()


def avrg_phase():
    temp_data = database.data.copy()
    for x in range(len(temp_data)):
        if temp_data[x] >= 0 and temp_data[x] <= 200:
            temp_data[x] = 0
        elif temp_data[x] > 200 and temp_data[x] <= 500:
            temp_data[x] = 1
        elif temp_data[x] > 500 and temp_data[x] < 800:
            temp_data[x] = 2
        elif temp_data[x] >= 800 and temp_data[x] <= 1000:
            temp_data[x] = 3
    n = len(temp_data) // 2
    while n:
        for i, el in enumerate(temp_data):
            while i >= n and temp_data[i - n] > el:
                temp_data[i] = temp_data[i - n]
                i -= n
            temp_data[i] = el
        n = 1 if n == 2 else int(n * 5.0 / 11)
    j=0
    results = [0,0,0,0]
    for x in range(4):
        for y in range(j,len(temp_data)):
            if temp_data[j] == x:
                results[x] += 1
                j += 1
            else:
                break

    print("FMR: " + str(results[0]))
    print("FM: " + str(results[1]))
    print("FMA: " + str(results[2]))
    print("FME: " + str(results[3]))
    input("\nPresione enter para continuar")


def avrg_phaseS(filename):
    temp_data = database.data.copy()
    for x in range(len(temp_data)):
        if temp_data[x] >= 0 and temp_data[x] <= 200:
            temp_data[x] = 0
        elif temp_data[x] > 200 and temp_data[x] <= 500:
            temp_data[x] = 1
        elif temp_data[x] > 500 and temp_data[x] < 800:
            temp_data[x] = 2
        elif temp_data[x] >= 800 and temp_data[x] <= 1000:
            temp_data[x] = 3
    n = len(temp_data) // 2
    while n:
        for i, el in enumerate(temp_data):
            while i >= n and temp_data[i - n] > el:
                temp_data[i] = temp_data[i - n]
                i -= n
            temp_data[i] = el
        n = 1 if n == 2 else int(n * 5.0 / 11)
    j=0
    results = [0,0,0,0]
    for x in range(4):
        for y in range(j,len(temp_data)):
            if temp_data[j] == x:
                results[x] += 1
                j += 1
            else:
                break
    file = open(filename, 'w+')
    file.write("Fase,Frecuencia\n")
    file.write("FMR," + str(results[0])+'\n')
    file.write("FM," + str(results[1]) + '\n')
    file.write("FMA," + str(results[2]) + '\n')
    file.write("FME," + str(results[3]) + '\n')
    file.close()


def most_phase():
    temp_data = database.data.copy()
    for x in range(len(temp_data)):
        if temp_data[x] >= 0 and temp_data[x] <= 200:
            temp_data[x] = 0
        elif temp_data[x] > 200 and temp_data[x] <= 500:
            temp_data[x] = 1
        elif temp_data[x] > 500 and temp_data[x] < 800:
            temp_data[x] = 2
        elif temp_data[x] >= 800 and temp_data[x] <= 1000:
            temp_data[x] = 3
    counter = [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]]
    for x in range(0,len(temp_data),10):
        for y in range(10):
            temp = x+y
            if temp_data[temp] == 0:
                counter[y][0]+=1
            elif temp_data[temp] == 1:
                counter[y][1] += 1
            elif temp_data[temp] == 2:
                counter[y][2] += 1
            elif temp_data[temp] == 3:
                counter[y][3] += 1
    print("Frecuencia de Fases en cada proceso: ")
    for x in range(10):
        print("Proceso P"+str(x+1))
        print("Fase\tFrecuencia")
        print("FMR\t"+str(counter[x][0]))
        print("FM\t" + str(counter[x][1]))
        print("FMA\t" + str(counter[x][2]))
        print("FME\t" + str(counter[x][3]))
    temp = [0,0]
    print("Fase con mayor frecuencia en cada proceso: ")
    for x in range(10):
        temp[0]=counter[x][0]
        temp[1]=0
        for y in range(1,4):
            if temp[0] < counter[x][y]:
                temp[0] = counter[x][y]
                temp[1] = y
        print("P"+str(x+1),end='\t')
        if temp[1] == 0:
            print("FMR")
        elif temp[1] == 1:
            print("FM")
        elif temp[1] == 2:
            print("FMA")
        elif temp[1] == 3:
            print("FME")
    input("Presione enter para continuar")


def most_phaseS(filename):
    temp_data = database.data.copy()
    for x in range(len(temp_data)):
        if temp_data[x] >= 0 and temp_data[x] <= 200:
            temp_data[x] = 0
        elif temp_data[x] > 200 and temp_data[x] <= 500:
            temp_data[x] = 1
        elif temp_data[x] > 500 and temp_data[x] < 800:
            temp_data[x] = 2
        elif temp_data[x] >= 800 and temp_data[x] <= 1000:
            temp_data[x] = 3
    counter = [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]]
    for x in range(0,len(temp_data),10):
        for y in range(10):
            temp = x+y
            if temp_data[temp] == 0:
                counter[y][0]+=1
            elif temp_data[temp] == 1:
                counter[y][1] += 1
            elif temp_data[temp] == 2:
                counter[y][2] += 1
            elif temp_data[temp] == 3:
                counter[y][3] += 1
    file = open(filename,'w+')
    file.write("Frecuencia de Fases en cada proceso: \n")
    for x in range(10):
        file.write("Proceso P"+str(x+1)+'\n')
        file.write("FMR," + str(counter[x][0])+'\n')
        file.write("FM," + str(counter[x][1]) + '\n')
        file.write("FMA," + str(counter[x][2]) + '\n')
        file.write("FME," + str(counter[x][3]) + '\n')

    temp = [0,0]
    file.write("Fase con mayor frecuencia en cada proceso: \n")
    for x in range(10):
        temp[0]=counter[x][0]
        temp[1]=0
        for y in range(1,4):
            if temp[0] < counter[x][y]:
                temp[0] = counter[x][y]
                temp[1] = y
        print()
        if temp[1] == 0:
            file.write("P"+str(x+1)+','+"FMR"+'\n')
        elif temp[1] == 1:
            file.write("P" + str(x + 1) + ',' + "FM"+'\n')
        elif temp[1] == 2:
            file.write("P" + str(x + 1) + ',' + "FMA"+'\n' )
        elif temp[1] == 3:
            file.write("P" + str(x + 1) + ',' + "FME"+'\n')
    file.close()


database = Archive("prac1-valores.csv")
main_menu = Menu()
while main_menu.menu_op != 1:
    main_menu.show_menu()
    main_menu.selection()
