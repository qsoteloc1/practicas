void most_frequent()
{
    int temp_data[t_data], results[2][1000];
    make_temp(temp_data);
    shell_sort(temp_data);
    int j=0;
    for(int x=1;x<=1000;x++)
    {
        results[0][x-1]=0;
        results[1][x-1]=x;
        for(int y=j;y<t_data;y++)
        {
            if(temp_data[y]==x)
                results[0][x-1]++;
            else{
                j=y;
                break;
            }
        }
    }
    for(int x=0;x<1000;x++)
    {
        int y=x;
        while((results[0][y]<results[0][y-1])&&y>=1)
        {
            swap(results[0][y],results[0][y-1]);
            swap(results[1][y],results[1][y-1]);
            y--;
        }
    }
    cout<<"Numero\tFrecuencia"<<endl;
    for(int x=999;x>=900;x--)
    {
        cout<<results[1][x]<<"\t"<<results[0][x]<<endl;
    }
    cout<<endl<<"Presione enter para continuar"<<endl;
    cin.get();
    cin.get();
    system("cls");
}

void less_frequent()
{
    int temp_data[t_data], results[2][1000];
    make_temp(temp_data);
    shell_sort(temp_data);
    int j=0;
    for(int x=1;x<=1000;x++)
    {
        results[0][x-1]=0;
        results[1][x-1]=x;
        for(int y=j;y<t_data;y++)
        {
            if(temp_data[y]==x)
                results[0][x-1]++;
            else{
                j=y;
                break;
            }
        }
    }
    for(int x=0;x<1000;x++)
    {
        int y=x;
        while((results[0][y]<results[0][y-1])&&y>=1)
        {
            swap(results[0][y],results[0][y-1]);
            swap(results[1][y],results[1][y-1]);
            y--;
        }
    }
    cout<<"Numero\tFrecuencia"<<endl;
    for(int x=0;x<100;x++)
    {
        cout<<results[1][x]<<"\t"<<results[0][x]<<endl;
    }
    cout<<endl<<"Presione enter para continuar"<<endl;
    cin.get();
    cin.get();
    system("cls");
}
