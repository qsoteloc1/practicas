void swap(int &small_n, int &big_n)
{
    int temp = small_n;
    small_n = big_n;
    big_n = temp;
}

void make_temp(int temp_data[])
{
    for(int x=0;x<t_data;x++)
    {
        temp_data[x]=data[x];
    }
}

void bubble_sort()
{
    int temp_data[t_data];
    make_temp(temp_data);
    clock_t t;
    t=clock();
    for(int x=0;x<t_data-1;x++)
    {
        for(int y=0;y<t_data-1-x;y++)
        {
            if(temp_data[y]>temp_data[y+1])
            {
                swap(temp_data[y],temp_data[y+1]);
            }
        }
    }
    t=(clock()-t)/CLOCKS_PER_SEC;
    cout<<"Los datos han sido ordenados"<<endl
        <<"El algoritmo de ordenamiento \"Bubble Sort\" ha tardado:"<<endl
        <<t<<" segundos en ordenar 100000 datos"<<endl
        <<"¿Mostrar datos?"<<endl
        <<"1.-Si"<<endl
        <<"2.-No"<<endl;
    int opcion;
    cin>>opcion;
    if(opcion==1)
    {
        for(int x=0;x<t_data;x++)
            cout<<temp_data[x]<<"\t";
    }
    cout<<endl<<"Presione enter para continuar"<<endl;
    cin.get();
    cin.get();
    system("cls");
}

void insertion_sort()
{
    int temp_data[t_data];
    make_temp(temp_data);
    clock_t t;
    t=clock();
    for(int x=1;x<t_data;x++)
    {
        int y=x;
        while((temp_data[y]<temp_data[y-1])&&y>=1)
        {
            swap(temp_data[y],temp_data[y-1]);
            y--;
        }
    }
    t=(clock()-t)/CLOCKS_PER_SEC;
    cout<<"Los datos han sido ordenados"<<endl
        <<"El algoritmo de ordenamiento \"Insertion Sort\" ha tardado:"<<endl
        <<t<<" segundos en ordenar 100000 datos"<<endl
        <<"¿Mostrar datos?"<<endl
        <<"1.-Si"<<endl
        <<"2.-No"<<endl;
    int opcion;
    cin>>opcion;
    if(opcion==1)
    {
        for(int x=0;x<t_data;x++)
            cout<<temp_data[x]<<"\t";
    }
    cout<<endl<<"Presione enter para continuar"<<endl;
    cin.get();
    cin.get();
    system("cls");
}

void shell_sort()
{
    int temp_data[t_data];
    make_temp(temp_data);
    clock_t t;
    t=clock();
    for(int s=t_data/2;s>0;s/=2)
    {
        for(int t=s;t<t_data;t++)
        {
            for(int j=t; j>=s && temp_data[j-s]>temp_data[j];j-=s)
            {
                swap(temp_data[j-s],temp_data[j]);
            }
        }
    }
    t=(clock()-t)/CLOCKS_PER_SEC;
    cout<<"Los datos han sido ordenados"<<endl
        <<"El algoritmo de ordenamiento \"Shell Sort\" ha tardado:"<<endl
        <<t<<" segundos en ordenar 100000 datos"<<endl
        <<"¿Mostrar datos?"<<endl
        <<"1.-Si"<<endl
        <<"2.-No"<<endl;
    int opcion;
    cin>>opcion;
    if(opcion==1)
    {
        for(int x=0;x<t_data;x++)
            cout<<temp_data[x]<<"\t";
    }
    cout<<endl<<"Presione enter para continuar"<<endl;
    cin.get();
    cin.get();
    system("cls");
}

//Metodos para ordenamiento general

void bubble_sort(int temp_data[])
{
    for(int x=0;x<t_data-1;x++)
    {
        for(int y=0;y<t_data-1-x;y++)
        {
            if(temp_data[y]>temp_data[y+1])
            {
                swap(temp_data[y],temp_data[y+1]);
            }
        }
    }
}

void insertion_sort(int temp_data[])
{
    for(int x=1;x<t_data;x++)
    {
        int y=x;
        while((temp_data[y]<temp_data[y-1])&&y>=1)
        {
            swap(temp_data[y],temp_data[y-1]);
            y--;
        }
    }
}

void shell_sort(int temp_data[])
{
    for(int s=t_data/2;s>0;s/=2)
    {
        for(int t=s;t<t_data;t++)
        {
            for(int j=t; j>=s && temp_data[j-s]>temp_data[j];j-=s)
            {
                swap(temp_data[j-s],temp_data[j]);
            }
        }
    }
}
