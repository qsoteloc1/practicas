void avrg_error()
{
    int temp_data[t_data], averange=0,counter[10]={0,0,0,0,0,0,0,0,0,0};
    make_temp(temp_data);
    for(int x=0;x<t_data;x++)
    {
        averange+=temp_data[x];
    }
    averange/=t_data;
    cout<<"El numero promedio de error es: "<<averange<<endl;
    for(int x=0;x<t_data;x+=10)
    {
        for(int y=0;y<10;y++)
        {
            counter[y]+=temp_data[x+y];
        }
    }
    for(int x=0;x<10;x++)
        counter[x]/=(t_data/10);

    cout<<"Promedio de error por fase"<<endl;
    cout<<"F1\tF2\tF3\tF4\tF5\tF6\tF7\tF8\tF9\tF10"<<endl;
    for(int x=0;x<10;x++)
        cout<<counter[x]<<"\t";
    cout<<endl;
    cout<<endl<<"Presione enter para continuar"<<endl;
    cin.get();
    cin.get();
    system("cls");
}

void avrg_phase()
{
    int temp_data[t_data];
    make_temp(temp_data);

    for(int x=0;x<t_data;x++)
    {
        if(temp_data[x]>=0&&temp_data[x]<=200)
            temp_data[x]=0;
        else if(temp_data[x]>200&&temp_data[x]<=500)
            temp_data[x]=1;
        else if(temp_data[x]>500&&temp_data[x]<800)
            temp_data[x]=2;
        else if(temp_data[x]>=800&&temp_data[x]<=1000)
            temp_data[x]=3;
    }

    shell_sort(temp_data);
    int j=0, results[4];
    for(int x=0;x<4;x++)
    {
        results[x]=0;
        for(int y=j;y<t_data;y++)
        {
            if(temp_data[y]==x)
                results[x]++;
            else{
                j=y;
                break;
            }
        }
    }
    cout<<"Fase\tFrecuencia"<<endl
        <<"FMR\t"<<results[0]<<endl
        <<"FM\t"<<results[1]<<endl
        <<"FMA\t"<<results[2]<<endl
        <<"FME\t"<<results[3]<<endl;

    cout<<endl<<"Presione enter para continuar"<<endl;
    cin.get();
    cin.get();
    system("cls");
}

void most_phase()
{
    int temp_data[t_data];
    make_temp(temp_data);

    for(int x=0;x<t_data;x++)
    {
        if(temp_data[x]>=0&&temp_data[x]<=200)
            temp_data[x]=0;
        else if(temp_data[x]>200&&temp_data[x]<=500)
            temp_data[x]=1;
        else if(temp_data[x]>500&&temp_data[x]<800)
            temp_data[x]=2;
        else if(temp_data[x]>=800&&temp_data[x]<=1000)
            temp_data[x]=3;
    }

    int counter[10][4];
    for(int x=0;x<10;x++)
        for(int y=0;y<4;y++)
        counter[x][y]=0;

    for(int x=0;x<t_data;x+=10)
    {
        for(int y=0;y<10;y++)
        {
            int temp=x+y;
            switch(temp_data[temp])
            {
                case 0:
                    counter[y][0]++;
                break;
                case 1:
                    counter[y][1]++;
                break;
                case 2:
                    counter[y][2]++;
                break;
                case 3:
                    counter[y][3]++;
                break;
            }
        }
    }
    cout<<"Frecuencia de Fases en cada proceso:"<<endl<<endl;
    for(int x=0;x<10;x++)
    {
        cout<<"Proceso P"<<x+1<<":"<<endl
            <<"Fase\tFrecuecia"<<endl
            <<"FMR\t"<<counter[x][0]<<endl
            <<"FM\t"<<counter[x][1]<<endl
            <<"FMA\t"<<counter[x][2]<<endl
            <<"FME\t"<<counter[x][3]<<endl<<endl;
    }
    int temp[2]={0,0};
    cout<<"Fase con mayor frecuencia en cada proceso:"<<endl<<endl;
    for(int x=0;x<10;x++)
    {
        temp[0]=counter[x][0];
        temp[1]=0;
        for(int y=1;y<4;y++)
        {
            if(temp[0]<counter[x][y])
            {
                temp[0]=counter[x][y];
                temp[1]=y;
            }

        }
        cout<<"P"<<x+1<<"\t";
        switch(temp[1])
        {
            case 0:
                cout<<"FMR"<<endl;
            break;
            case 1:
                cout<<"FM"<<endl;
            break;
            case 2:
                cout<<"FMA"<<endl;
            break;
            case 3:
                cout<<"FME"<<endl;
            break;
        }
    }

    cout<<endl<<"Presione enter para continuar"<<endl;
    cin.get();
    cin.get();
    system("cls");
}
