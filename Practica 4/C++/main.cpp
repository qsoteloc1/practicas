#include<iostream>
#include<fstream>
#include<cstdlib>
#include<ctime>
#include<windows.h>
#include<string>

#define t_data 100000

using namespace std;

int data[t_data];

#include"error.h"
#include"sorting.h"
#include"frequency.h"
#include"averange.h"
#include"save.h"

bool get_data()
{
    fstream csv("prac1-valores.csv", ios::in);
    if(!csv.is_open())
    {
        return false;
    }
    string line;
    int x=0;
    getline(csv,line);
    while(getline(csv,line,','))
    {
        data[x]=atoi(line.c_str());
        x++;
        if((x+1)%10==0)
        {
            getline(csv,line,'\n');
            data[x]=atoi(line.c_str());
            x++;
        }

    };
    csv.close();
    return true;
}

bool main_menu()
{
    int opcion;
    cout<<"1.-Salir"<<endl
        <<"2.-Guardar Resultados en un Archivo"<<endl
        <<"3.-Aplicar Ordenamiento \"Bubble Sort\""<<endl
        <<"4.-Aplicar Ordenamiento \"Insertion Sort\""<<endl
        <<"5.-Aplicar Ordenamiento \"Shell Sort\""<<endl
        <<"6.-Mostrar los 100 numeros de errores mas frecuentes"<<endl
        <<"7.-Mostrar los 100 numeros de errores menos frecuentes"<<endl
        <<"8.-Mostrar el numero promedio de error"<<endl
        <<"9.-Mostrar el numero de veces que se presento cada fase"<<endl
        <<"10.-Fase con mayor presentacion en el proceso"<<endl;
    cin>>opcion;
    system("cls");
    switch(opcion)
    {
        case 1:
            return false;
        break;
        case 2:
            save_data();
            return true;
        break;
        case 3:
            bubble_sort();
            return true;
        break;
        case 4:
            insertion_sort();
            return true;
        case 5:
            shell_sort();
            return true;
        break;
        case 6:
            most_frequent();
            return true;
        break;
        case 7:
            less_frequent();
            return true;
        break;
        case 8:
            avrg_error();
            return true;
        break;
        case 9:
            avrg_phase();
            return true;
        break;
        case 10:
            most_phase();
            return true;
        break;
        default:
            return true;
    }
}

int main()
{
    if(!get_data())
        return err_code1;

    while(main_menu()){ };
    return 0;
}
