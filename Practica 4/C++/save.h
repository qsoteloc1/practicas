void avrg_error(int temp_data[], string filename)
{
    fstream csv(filename.c_str(),ios::out|ios::trunc);
    int averange=0,counter[10]={0,0,0,0,0,0,0,0,0,0};
    for(int x=0;x<t_data;x++)
    {
        averange+=temp_data[x];
    }
    averange/=t_data;
    csv<<"Promedio de error"<<endl;
    csv<<"Promedio General:,"<<averange<<endl;
    for(int x=0;x<t_data;x+=10)
    {
        for(int y=0;y<10;y++)
        {
            counter[y]+=temp_data[x+y];
        }
    }
    for(int x=0;x<10;x++)
        counter[x]/=(t_data/10);

    csv<<"Promedio de error por fase"<<endl;
    csv<<"F1,F2,F3,F4,F5,F6,F7,F8,F9,F10"<<endl;
    for(int x=0;x<9;x++)
        csv<<counter[x]<<",";
    csv<<counter[9]<<"\n";
    csv.close();
}

void avrg_phase(int temp_data[], string filename)
{
    fstream csv(filename.c_str(),ios::out|ios::trunc);
    for(int x=0;x<t_data;x++)
    {
        if(temp_data[x]>=0&&temp_data[x]<=200)
            temp_data[x]=0;
        else if(temp_data[x]>200&&temp_data[x]<=500)
            temp_data[x]=1;
        else if(temp_data[x]>500&&temp_data[x]<800)
            temp_data[x]=2;
        else if(temp_data[x]>=800&&temp_data[x]<=1000)
            temp_data[x]=3;
    }

    shell_sort(temp_data);
    int j=0, results[4];
    for(int x=0;x<4;x++)
    {
        results[x]=0;
        for(int y=j;y<t_data;y++)
        {
            if(temp_data[y]==x)
                results[x]++;
            else{
                j=y;
                break;
            }
        }
    }
    csv<<"Frecuencia de Fases"<<endl;
    csv<<"FMR,"<<results[0]<<endl;
    csv<<"FM,"<<results[1]<<endl;
    csv<<"FMA,"<<results[2]<<endl;
    csv<<"FME,"<<results[3]<<endl;
    csv.close();
}

void most_phase(int temp_data[], string filename)
{
    fstream csv(filename.c_str(),ios::out|ios::trunc);
    for(int x=0;x<t_data;x++)
    {
        if(temp_data[x]>=0&&temp_data[x]<=200)
            temp_data[x]=0;
        else if(temp_data[x]>200&&temp_data[x]<=500)
            temp_data[x]=1;
        else if(temp_data[x]>500&&temp_data[x]<800)
            temp_data[x]=2;
        else if(temp_data[x]>=800&&temp_data[x]<=1000)
            temp_data[x]=3;
    }

    int counter[10][4];
    for(int x=0;x<10;x++)
        for(int y=0;y<4;y++)
        counter[x][y]=0;

    for(int x=0;x<t_data;x+=10)
    {
        for(int y=0;y<10;y++)
        {
            int temp=x+y;
            switch(temp_data[temp])
            {
                case 0:
                    counter[y][0]++;
                break;
                case 1:
                    counter[y][1]++;
                break;
                case 2:
                    counter[y][2]++;
                break;
                case 3:
                    counter[y][3]++;
                break;
            }
        }
    }
    csv<<"Presentacion de fases por procesos"<<endl<<endl;

    for(int x=0;x<10;x++)
    {
        csv<<"En el proceso P"<<x+1<<":"<<endl
            <<"Fase,Frecuecia"<<endl
            <<"FMR,"<<counter[x][0]<<endl
            <<"FM,"<<counter[x][1]<<endl
            <<"FMA,"<<counter[x][2]<<endl
            <<"FME,"<<counter[x][3]<<endl<<endl;
    }
    int temp[2]={0,0};
    csv<<"Mayor presentacion en el proceso"<<endl;
    for(int x=0;x<10;x++)
    {
        temp[0]=counter[x][0];
        temp[1]=0;
        for(int y=1;y<4;y++)
        {
            if(temp[0]<counter[x][y])
            {
                temp[0]=counter[x][y];
                temp[1]=y;
            }

        }
        csv<<"P"<<x<<",";
        switch(temp[1])
        {
            case 0:
                csv<<"FMR"<<endl;
            break;
            case 1:
                csv<<"FM"<<endl;
            break;
            case 2:
                csv<<"FMA"<<endl;
            break;
            case 3:
                csv<<"FME"<<endl;
            break;
        }
    }
    csv.close();
}


void most_frequent(int temp_data[], string filename)
{
    int results[2][1000];
    shell_sort(temp_data);
    int j=0;
    for(int x=1;x<=1000;x++)
    {
        results[0][x-1]=0;
        results[1][x-1]=x;
        for(int y=j;y<t_data;y++)
        {
            if(temp_data[y]==x)
                results[0][x-1]++;
            else{
                j=y;
                break;
            }
        }
    }
    for(int x=0;x<1000;x++)
    {
        int y=x;
        while((results[0][y]<results[0][y-1])&&y>=1)
        {
            swap(results[0][y],results[0][y-1]);
            swap(results[1][y],results[1][y-1]);
            y--;
        }
    }
    fstream csv(filename.c_str(),ios::out|ios::trunc);
    csv<<"Numero,Frecuencia"<<endl;
    for(int x=999;x>=900;x--)
    {
        csv<<results[1][x]<<","<<results[0][x]<<endl;
    }
}

void less_frequent(int temp_data[], string filename)
{
    int results[2][1000];
    shell_sort(temp_data);
    int j=0;
    for(int x=1;x<=1000;x++)
    {
        results[0][x-1]=0;
        results[1][x-1]=x;
        for(int y=j;y<t_data;y++)
        {
            if(temp_data[y]==x)
                results[0][x-1]++;
            else{
                j=y;
                break;
            }
        }
    }
    for(int x=0;x<1000;x++)
    {
        int y=x;
        while((results[0][y]<results[0][y-1])&&y>=1)
        {
            swap(results[0][y],results[0][y-1]);
            swap(results[1][y],results[1][y-1]);
            y--;
        }
    }
    fstream csv(filename.c_str(),ios::out|ios::trunc);
    csv<<"Numero,Frecuencia"<<endl;
    for(int x=0;x<100;x++)
    {
        csv<<results[1][x]<<","<<results[0][x]<<endl;
    }
}


void writetofile(int temp_data[], string filename)
{
    fstream csv(filename.c_str(),ios::out|ios::trunc);
    csv<<"Numero,Dato\n";
    for(int x=0;x<t_data;x++)
    {
        csv<<x+1<<","<<temp_data[x]<<"\n";
    }
    csv.close();
}

void save_data()
{
    int temp_data[t_data];
    make_temp(temp_data);
    string name;
    cout<<"Nombre del archivo: ";
    cin>>name;
    if(name.find('.')!=string::npos)
    {
        if(name.substr(name.find('.')+1,name.size())!="csv")
        {
            name.erase(name.find('.'), name.size());
            name.append(".csv");
        }
    }
    else
    {
        name.append(".csv");
    }
    cout<<"�Que opcion desea guadar?"<<endl
        <<"1.- Ordenamiento \"Bubble Sort\""<<endl
        <<"2.- Ordenamiento \"Insertion Sort\""<<endl
        <<"3.- Ordenamiento \"Shell Sort\""<<endl
        <<"4.- 100 Numeros de error mas frecuentes"<<endl
        <<"5.- 100 Numeros de error menos frecuentes"<<endl
        <<"6.- Numero Promedio de error"<<endl
        <<"7.- Numero de veces que se presento la fase"<<endl
        <<"8.- Fase con mayor presentacion en el proceso"<<endl;
    int opcion;
    cin>>opcion;
    switch(opcion)
    {
        case 1:
            bubble_sort(temp_data);
            writetofile(temp_data, name);
        break;
        case 2:
            insertion_sort(temp_data);
            writetofile(temp_data, name);
        break;
        case 3:
            shell_sort(temp_data);
            writetofile(temp_data, name);
        break;
        case 4:
            most_frequent(temp_data, name);
        break;
        case 5:
            less_frequent(temp_data,name);
        break;
        case 6:
            avrg_error(temp_data,name);
        break;
        case 7:
            avrg_phase(temp_data,name);
        break;
        case 8:
            most_phase(temp_data,name);
        break;
    }
    system("cls");
}
