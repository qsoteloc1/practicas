import datetime

dbP_N = ["administrativos.csv","enfermeros.csv","doctores.csv","pacientes.csv"]
dbV_N = ["autos_compactos.csv","ambulancias.csv","tractocamiones.csv","camionetas.csv","motocicletas.csv"]
dbP_H = "Clave,Nombre,Apellido Paterno,Apellido Materno,Fecha de Nacimiento,Edad,Hora de Alta,Curp,RFC," \
        "Tipo de Sangre,Padecimientos,Consume Estupefacientes,Enfermedades Cronicas Degenerativas,Descripcion de " \
        "Enfermedades Cronicas Degenerativas\n"
dbV_H = "Placas,Tipo,Marca,Modelo,Rodado,Numero de Llantas,Transmicion,Cilindraje,Numero de Puertas,Asignado a\n"
personas = []
vehiculos = []

def initial_check():
    for database in dbP_N:
        try:
            open(database,'r')
        except IOError:
            csv = open(database,'w+')
            csv.write(dbP_H)
            csv.close()
        else:
            csv = open(database,'r')
            if csv.readline() != dbP_H:
                csv.close()
                exit(1)
    for database in dbV_N:
        try:
            open(database,'r')
        except IOError:
            csv = open(database,'w+')
            csv.write(dbV_H)
            csv.close()
        else:
            csv = open(database,'r')
            if csv.readline() != dbV_H:
                csv.close()
                exit(1)


def read_data():
    x = 0
    for database in dbP_N:
        personas.append([])
        csv = open(database, 'r')
        csv.readline()
        line = csv.readline()
        while line != "":
            line = line.replace('\n','')
            line = list(line.split(','))
            personas[x].append(line)
            line = csv.readline()
        x += 1
    x = 0
    for database in dbV_N:
        vehiculos.append([])
        csv = open(database, 'r')
        csv.readline()
        line = csv.readline()
        while line != "":
            line = line.replace('\n', '')
            line = list(line.split(','))
            vehiculos[x].append(line)
            line = csv.readline()
        x += 1


def write_data():
    x = 0
    for database in dbP_N:
        csv = open(database, 'w+')
        csv.write(dbP_H)
        for element in personas[x]:
            line = ""
            for data in element:
                line += str(data)+','
            line = line[0:len(line)-1]
            csv.write(line+'\n')
        x += 1

    x = 0
    for database in dbV_N:
        csv = open(database, 'w+')
        csv.write(dbV_H)
        for element in vehiculos[x]:
            line = ""
            for data in element:
                line += str(data) + ','
            line = line[0:len(line) - 1]
            csv.write(line + '\n')
        x += 1


def busqueda():
    dato = input("Clave/Placa: ")
    if dato.isdigit():
        dato=int(dato)
        for database in personas:
            for element in database:
                if int(element[0]) == dato:
                    print(element)
                    return str(dato)
        print("No existe registro para la clave: "+str(dato))
        return False
    else:
        for database in vehiculos:
            for element in database:
                if element[0] == dato:
                    print(element)
                    return dato
        print("No existe registro para la clave: "+str(dato))
        return False


def getID():
    count = 1
    for database in personas:
        for element in database:
            if int(element[0]) >= count:
                count = int(element[0])+1
    return count


def altaP(data):
    newPerson = []
    id = getID()
    print("Clave: "+str(id))
    newPerson.append(str(id))
    newPerson.append(input("Nombre: "))
    newPerson.append(input("Apellido Paterno: "))
    newPerson.append(input("Apellido Materno: "))
    day = int(input("Dia de Nacimiento (DD): "))
    month = int(input("Mes de Nacimiento (MM): "))
    year = int(input("Año de Nacimiento (AAAA): "))
    now =  datetime.datetime.now()
    now.year - year - ((now.month, now.day) < (month, day))
    newPerson.append(str(day)+"\\"+str(month)+"\\"+str(year))
    print("Edad: "+str(now.year - year - ((now.month, now.day) < (month, day))))
    newPerson.append(str(now.year - year - ((now.month, now.day) < (month, day))))
    print("Hora de Alta: "+str(now.hour)+":"+str(now.minute))
    newPerson.append(str(now.hour)+":"+str(now.minute))
    newPerson.append(input("Curp: "))
    newPerson.append(input("RFC: "))
    newPerson.append(input("Tipo de Sangre (O+):"))
    print("Padecimientos: ")
    print("1.-Si ")
    print("2.-No ")
    temp = int(input(("Opcion: ")))
    if temp == 1:
        newPerson.append(input("¿Cuales?"))
    else:
        newPerson.append("No")
    print("¿Consume Estupefacientes?: ")
    print("1.-Si ")
    print("2.-No ")
    temp = int(input(("Opcion: ")))
    if temp == 1:
        newPerson.append("Si")
    else:
        newPerson.append("No")
    print("Tiene Enfermedades Cronicas Degenerativas: ")
    print("1.-Si ")
    print("2.-No ")
    temp = int(input(("Opcion: ")))
    if temp == 1:
        newPerson.append(input("¿Cuales?: "))
        newPerson.append(input("Descripcion de las Enfermedades: "))
    else:
        newPerson.append("No")
        newPerson.append("No")
    print("¿Estos datos son correctos?: ")
    print("1.-Si ")
    print("2.-No ")
    temp = int(input(("Opcion: ")))
    if temp == 1:
        personas[data].append(newPerson)


def altaV(data):
    newVehicule = []
    newVehicule.append(input("Placas: "))
    print("Tipo: "+str(dbV_N[data].replace(".csv", "")))
    newVehicule.append(str(dbV_N[data].replace(".csv", "")))
    newVehicule.append(input("Marca: "))
    newVehicule.append(input("Modelo: "))
    newVehicule.append(input("Rodado: "))
    newVehicule.append(input("Numero de Llantas: "))
    print("Transmicion: ")
    print("1.-Manual")
    print("2.-Semi-Automatica")
    print("3.-Automatica")
    temp = int(input("Opcion: "))
    if temp == 1:
        newVehicule.append("Manual")
    elif temp == 2:
        newVehicule.append("Semi-Automatica")
    elif temp == 3:
        newVehicule.append("Automatica")
    newVehicule.append(input("Cilindraje: "))
    newVehicule.append(input("Numero de Puertas: "))
    newVehicule.append(input("Asigando a: "))
    print("¿Estos datos son correctos?: ")
    print("1.-Si ")
    print("2.-No ")
    temp = int(input("Opcion: "))
    if temp == 1:
        vehiculos[data].append(newVehicule)


def alta():
    print("1.-Adminstrativos")
    print("2.-Enfermeros")
    print("3.-Doctores")
    print("4.-Pacientes")
    print("5.-Autos Compactos")
    print("6.-Ambulancias")
    print("7.-Tractocamiones")
    print("8.-Camionetas")
    print("9.-Motocicletas")
    data = int(input("Opcion: "))
    if data>0 and data<5:
        altaP(int(data-1))
    elif data>4 and data<10:
        altaV(data-5)

def modificarP(dato):
    x = 0
    for database in personas:
        y = 0
        for element in database:
            if int(element[0]) == dato:
                newPerson = []
                print("Clave: " + str(dato))
                newPerson.append(str(dato))
                newPerson.append(input("Nombre: "))
                newPerson.append(input("Apellido Paterno: "))
                newPerson.append(input("Apellido Materno: "))
                day = int(input("Dia de Nacimiento (DD): "))
                month = int(input("Mes de Nacimiento (MM): "))
                year = int(input("Año de Nacimiento (AAAA): "))
                now = datetime.datetime.now()
                now.year - year - ((now.month, now.day) < (month, day))
                newPerson.append(str(day) + "\\" + str(month) + "\\" + str(year))
                print("Edad: " + str(now.year - year - ((now.month, now.day) < (month, day))))
                newPerson.append(str(now.year - year - ((now.month, now.day) < (month, day))))
                print("Hora de Alta: " + str(now.hour) + ":" + str(now.minute))
                newPerson.append(input("Curp: "))
                newPerson.append(input("RFC: "))
                newPerson.append(input("Tipo de Sangre (O+):"))
                print("Padecimientos: ")
                print("1.-Si ")
                print("2.-No ")
                temp = int(input(("Opcion: ")))
                if temp == 1:
                    newPerson.append(input("¿Cuales?"))
                else:
                    newPerson.append("No")
                print("¿Consume Estupefacientes?: ")
                print("1.-Si ")
                print("2.-No ")
                temp = int(input(("Opcion: ")))
                if temp == 1:
                    newPerson.append("Si")
                else:
                    newPerson.append("No")
                print("Tiene Enfermedades Cronicas Degenerativas: ")
                print("1.-Si ")
                print("2.-No ")
                temp = int(input(("Opcion: ")))
                if temp == 1:
                    newPerson.append(input("¿Cuales?: "))
                    newPerson.append(input("Descripcion de las Enfermedades: "))
                else:
                    newPerson.append("No")
                    newPerson.append("No")
                print("¿Estos datos son correctos?: ")
                print("1.-Si ")
                print("2.-No ")
                temp = int(input(("Opcion: ")))
                if temp == 1:
                    personas[x][y]= newPerson
            y+=1
        x+=1


def modificarV(dato):
    x = 0
    for database in vehiculos:
        y = 0
        for element in database:
            if element[0] == dato:
                newVehicule = []
                print("Placas: "+str(dato))
                newVehicule.append(str(dato))
                print("Tipo: " + str(dbV_N[x].replace(".csv", "")))
                newVehicule.append(str(dbV_N[x].replace(".csv", "")))
                newVehicule.append(input("Marca: "))
                newVehicule.append(input("Modelo: "))
                newVehicule.append(input("Rodado: "))
                newVehicule.append(input("Numero de Llantas: "))
                print("Transmicion: ")
                print("1.-Manual")
                print("2.-Semi-Automatica")
                print("3.-Automatica")
                temp = int(input("Opcion: "))
                if temp == 1:
                    newVehicule.append("Manual")
                elif temp == 2:
                    newVehicule.append("Semi-Automatica")
                elif temp == 3:
                    newVehicule.append("Automatica")
                newVehicule.append(input("Cilindraje: "))
                newVehicule.append(input("Numero de Puertas: "))
                newVehicule.append(input("Asigando a: "))
                print("¿Estos datos son correctos?: ")
                print("1.-Si ")
                print("2.-No ")
                temp = int(input("Opcion: "))
                if temp == 1:
                    vehiculos[x][y] = newVehicule

            y+=1
        x+=1

def modificar():
    dato = busqueda()
    if dato != False:
        if dato.isdigit():
            print("¿Editar este registro?")
            print("1.-Si ")
            print("2.-No ")
            temp = int(input("Opcion: "))
            if temp == 1:
                modificarP(int(dato))
        else:
            print("¿Editar este registro?")
            print("1.-Si ")
            print("2.-No ")
            temp = int(input("Opcion: "))
            if temp == 1:
                modificarV(dato)


def eliminarP(dato):
    found = []
    for database in personas:
        for element in database:
            if int(element[0]) == dato:
                database.remove(element)


def eliminarV(dato):
    for database in vehiculos:
        for element in database:
            if element[0] == dato:
                database.remove(element)


def eliminar():
    dato = busqueda()
    if dato != False:
        if dato.isdigit():
            print("¿Eliminar este registro?")
            print("1.-Si ")
            print("2.-No ")
            temp = int(input("Opcion: "))
            if temp == 1:
                eliminarP(int(dato))
        else:
            print("¿Eliminar este registro?")
            print("1.-Si ")
            print("2.-No ")
            temp = int(input("Opcion: "))
            if temp == 1:
                eliminarV(dato)


def mostrar():
    x=0
    print("Personas entre 30 y 40 años")
    for database in personas:
        for element in database:
            if int(element[5])>=30 and int(element[5])<=40:
                print("Clave: "+ str(element[0])+ " Nombre: "+str(element[1])+" "+str(element[2])+" "+str(element[3]))
        x+=1
        if x==3:
            return
                
initial_check()
read_data()
menu_op = 0
while menu_op != 1:
    print("1.-Salir")
    print("2.-Dar de Alta")
    print("3.-Busqueda")
    print("4.-Modificar")
    print("5.-Eliminar")
    print("6.-Mostrar empleados entre 30 y 40 años")
    menu_op = int(input("Opcion: "))
    if menu_op == 2:
        alta()
        write_data()
    elif menu_op == 3:
        busqueda()
        write_data()
    elif menu_op == 4:
        modificar()
        write_data()
    elif menu_op == 5:
        eliminar()
        write_data()
    elif menu_op == 6:
        mostrar()
        write_data()
