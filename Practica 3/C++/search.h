/*
Busqueda
Status: Terminado
*/

bool busquedaP(unsigned int clave)
{
    system("cls");
    for(int x=0;x<4;x++)
    {
        persona *siguiente = baseP[x].sgt;
        while(siguiente->sgt!=NULL)
        {
            if(siguiente->clave==clave)
            {
                cout<<"Clave: "<<siguiente->clave<<endl
                    <<"Nombre: "<<siguiente->nombre<<endl
                    <<"Apellido Paterno: "<<siguiente->apellidoP<<endl
                    <<"Apellido Materno: "<<siguiente->apellidoM<<endl
                    <<"Fecha de Nacimiento: "<<siguiente->fechaNac<<endl
                    <<"Edad: "<<siguiente->edad<<endl
                    <<"Hora de Alta: "<<siguiente->horaAlta<<endl
                    <<"Curp: "<<siguiente->curp<<endl
                    <<"RFC: "<<siguiente->rfc<<endl
                    <<"Tipo de Sangre: "<<siguiente->sangre<<endl
                    <<"Padecimientos: "<<siguiente->padecimientos<<endl
                    <<"Consume Estupefacientes: "<<siguiente->estupefacientes<<endl
                    <<"Enfermedades Cronicas Degenerativas: "<<siguiente->enfCron<<endl
                    <<"Descripcion de Enfermedades Cronicas Degenerativas: "<<siguiente->enfCronD<<endl;
                return true;
            }
            siguiente=siguiente->sgt;
        }
    }
    cout<<"No existe el registro para la persona con la clave "<<clave<<endl;
    return false;
}

bool busquedaV(string placa) //Completo
{
    system("cls");
    for(int x=0;x<5;x++)
    {
        vehiculo *siguiente = baseV[x].sgt;
        while(siguiente->sgt!=NULL)
        {
            if(siguiente->placas==placa)
            {
                cout<<"Placas: "<<siguiente->placas<<endl
                    <<"Tipo: "<<siguiente->tipo<<endl
                    <<"Marca: "<<siguiente->marca<<endl
                    <<"Modelo: "<<siguiente->modelo<<endl
                    <<"Rodado: "<<siguiente->rodado<<endl
                    <<"Numero de Llantas: "<<siguiente->llantas<<endl
                    <<"Transmicion: "<<siguiente->transmicion<<endl
                    <<"Cilindraje: "<<siguiente->cilindraje<<endl
                    <<"Numero de Puertas: "<<siguiente->puertas<<endl
                    <<"Asignado a: "<<siguiente->asignado<<endl;
                return true;
            }
            siguiente=siguiente->sgt;
        }
    }
    cout<<"No existe el registro para el automovil con la placa "<<placa<<endl;
    return false;
}

bool is_number(string data) //Completo
{
    for(int x=0;x<data.size();x++)
    {
        if(!isdigit(data[x]))
           return false;
    }
    return true;
}

void busqueda() //Completo
{
    system("cls");
    string data;
    cout<<"Ingrese Clave/Placa: ";
    cin>>data;
    if(is_number(data))
    {
        busquedaP(atoi(data.c_str()));
        cin.get();
        cin.get();
    }else
    {
        busquedaV(data);
        cin.get();
        cin.get();
    }
}

