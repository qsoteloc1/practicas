/*
Alta
Status: Terminado
*/

int getId() //Completo
{
    int id=0;
    for(int x=0;x<4;x++)
    {
        persona *siguiente = baseP[x].sgt;
        while(siguiente->sgt != NULL)
        {
            if(siguiente->clave>id)
                id=siguiente->clave;
            siguiente=siguiente->sgt;
        }
    }
    return id+1;
}

void altaP(int db) //Completo
{
    system("cls");
    int temp;
    //Clave
    cout<<"Clave: ";
    tempP.clave = getId();
    cout<<tempP.clave<<endl;
    //Nombre
    cout<<"Nombre: ";
    cin>>tempP.nombre;
    //Apellido Paterno
    cout<<"Apellido Paterno: ";
    cin>>tempP.apellidoP;
    //Apellido Materno
    cout<<"Apellido Materno: ";
    cin>>tempP.apellidoM;
    //Fecha de Nacimiento
    string aux;
    int day,month,year;
    cout<<"Dia de Nacimiento (DD): ";
    cin>>day;
    cout<<"Mes de Nacimiento (MM): ";
    cin>>month;
    cout<<"A�o de Nacimiento (AAAA): ";
    cin>>year;
    ostringstream ss;
    ss<<day<<"\\"<<month<<"\\"<<year;
    aux = ss.str();
    strcpy(tempP.fechaNac,aux.c_str());
    //Edad
    time_t theTime = time(NULL);
    struct tm *aTime = localtime(&theTime);
    tempP.edad=0;
    if(month>aTime->tm_mon+1)
    {
        tempP.edad = aTime->tm_year + 1900 - year ;
    }else
    {
        tempP.edad = aTime->tm_year + 1900 - 1 - year;
    }
    cout<<"Edad: "<<tempP.edad<<endl;
    //Hora de Alta
    ss.str("");
    ss<<aTime->tm_hour<<":"<<aTime->tm_min;
    aux=ss.str();
    strcpy(tempP.horaAlta,aux.c_str());
    cout<<"Hora de Alta: "<<tempP.horaAlta<<endl;
    //Curp
    cout<<"Curp: ";
    cin>>tempP.curp;
    //Rfc
    cout<<"RFC: ";
    cin>>tempP.rfc;
    //Tipo de Sangre
    cout<<"Tipo de Sangre: (O+):";
    cin>>tempP.sangre;
    //Padecimientos
    cout<<"Padecimientos: "<<endl
        <<"1.-Si"<<endl
        <<"2.-No"<<endl;
    cin>>temp;
    if(temp==1)
    {
        cout<<"Descripcion de padecimientos: ";
        cin.get();
        getline(cin,tempP.padecimientos);
    }else
    {
        tempP.padecimientos="No";
    }
    //Estupefacientes
    cout<<"Consume Estupefacientes: "<<endl
        <<"1.-Si"<<endl
        <<"2.-No"<<endl;
    cin>>temp;
    if(temp==1)
        strcpy(tempP.estupefacientes,"Si");
    else
        strcpy(tempP.estupefacientes,"No");
    //Enfermedades Cronicas
    cout<<"Tiene enfermedades Cronicas Degenerativas?"<<endl
        <<"1.-Si"<<endl
        <<"2.-No"<<endl;
    cin>>temp;
    cin.get();
    if(temp==1)
    {
        cout<<"Enfermedades Cronicas Degenerativas: ";
        getline(cin,tempP.enfCron);
        cout<<"Descripcion de Enfermedades Cronicas Degenerativas: ";
        getline(cin,tempP.enfCronD);
    }else
    {
        tempP.enfCron="No";
        tempP.enfCronD="No";
    }
    //Correccion
    cout<<"�Estos datos son correctos?"<<endl
        <<"1.-Si"<<endl
        <<"2.-No"<<endl;
    cin>>temp;
    if(temp==1)
    {
        //Escribir en archivo
        persona *ultimo = baseP[db].sgt;
        while(ultimo->sgt != NULL)
        {
            ultimo = ultimo->sgt;
        }
        ultimo->sgt=new(persona);
        ultimo->clave = tempP.clave;
        ultimo->nombre = tempP.nombre;
        ultimo->apellidoP = tempP.apellidoP;
        ultimo->apellidoM = tempP.apellidoM;
        strcpy(ultimo->fechaNac,tempP.fechaNac);
        ultimo->edad = tempP.edad;
        strcpy(ultimo->horaAlta,tempP.horaAlta);
        strcpy(ultimo->curp,tempP.curp);
        strcpy(ultimo->rfc,tempP.rfc);
        strcpy(ultimo->sangre,tempP.sangre);
        ultimo->padecimientos = tempP.padecimientos;
        strcpy(ultimo->estupefacientes,tempP.estupefacientes);
        ultimo->enfCron = tempP.enfCron;
        ultimo->enfCronD = tempP.enfCronD;
        write_data();
    }
    return;
}

void altaV(int db) //Completo
{
    system("cls");
    int temp;
    //Placas
    cout<<"Placas: ";
    cin>>tempV.placas ;
    //Tipo
    tempV.tipo = dbV_N[db].substr(0,dbV_N[db].find('.'));
    cout<<"Tipo: "<< tempV.tipo<<endl;
    //Marca
    cout<<"Marca: ";
    cin>>tempV.marca;
    //Modelo
    cout<<"Modelo (AAAA): ";
    cin>>tempV.modelo;
    //Rodado
    cout<<"Rodado: ";
    cin>>tempV.rodado;
    //Numero de Llantas
    cout<<"Numero de Llantas: ";
    cin>>tempV.llantas;
    //Transmicion
    cout<<"Transmicion: "<<endl
        <<"1.-Manual"<<endl
        <<"2.-Semi-Automatica"<<endl
        <<"3.-Automatica"<<endl;
    cin>>temp;
    if(temp==1)
    {
        tempV.transmicion = "Manual";
    }else if(temp==2)
    {
        tempV.transmicion = "Semi-Automatica";
    }else
    {
        tempV.transmicion = "Automatica";
    }
    //Cilindraje
    cout<<"Cilindraje: ";
    cin>>tempV.cilindraje;
    //Numero de Puertas
    cout<<"Numero de Puertas: ";
    cin>>tempV.puertas;
    //Asignado a
    cout<<"Asignado a: ";
    cin>>tempV.asignado;
    //Correccion
    cout<<"�Estos datos son correctos?"<<endl
        <<"1.-Si"<<endl
        <<"2.-No"<<endl;
    cin>>temp;
    if(temp==1)
    {
        //Escribir en archivo
        vehiculo *ultimo = baseV[db].sgt;
        while(ultimo->sgt != NULL)
        {
            ultimo = ultimo->sgt;
        }
        ultimo->sgt=new(vehiculo);
        ultimo->placas = tempV.placas;
        ultimo->tipo = tempV.tipo;
        ultimo->marca = tempV.marca;
        ultimo->modelo = tempV.modelo;
        ultimo->llantas = tempV.llantas;
        ultimo->transmicion = tempV.transmicion;
        ultimo->cilindraje= tempV.cilindraje;
        ultimo->puertas = tempV.puertas;
        ultimo->asignado = tempV.asignado;
        write_data();
    }
    return;
}

void alta() //Completo
{
    system("cls");
    int option;
    cout<<"1.-Adminstrativo"<<endl
        <<"2.-Enfermero"<<endl
        <<"3.-Doctor"<<endl
        <<"4.-Paciente"<<endl
        <<"5.-Autos Compactos"<<endl
        <<"6.-Ambulancias"<<endl
        <<"7.-Tractocamiones"<<endl
        <<"8.-Camionetas"<<endl
        <<"9.-Motocicletas"<<endl
        <<"Opcion: ";
    cin>>option;
    if(option>0 && option<5)
        altaP(option-1);
    else if(option>4 && option <10)
        altaV(option-5);
}

