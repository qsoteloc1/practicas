#include<iostream>
#include<fstream>
#include<string>
#include<cstring>
#include<cstdlib>
#include<cstddef>
#include<ctime>
#include<sstream>

using namespace std;

struct persona
{
    string nombre, apellidoP, apellidoM, padecimientos, enfCron, enfCronD;
    char fechaNac[11], horaAlta[6], curp[19], rfc[14], sangre[3],estupefacientes[3];
    unsigned short int edad;
    unsigned int clave;
    persona *sgt = NULL;
}baseP[4],tempP;

struct vehiculo
{
    string placas, marca, asignado,transmicion, tipo;
    unsigned short int modelo, rodado, llantas, cilindraje, puertas;
    vehiculo *sgt = NULL;
}baseV[5],tempV;

string dbP_N[] = {"Administrativos.csv","Enfermeros.csv","Doctores.csv","Pacientes.csv"};
string dbV_N[] = {"Autos_Compactos.csv","Ambulancias.csv","Tractocamiones.csv","Camionetas.csv","Motocicletas.csv"};
string dbP_D = "Clave,Nombre,Apellido Paterno,Apellido Materno,Fecha de Nacimiento,Edad,Hora de Alta,Curp,RFC,Tipo de Sangre,Padecimientos,Consume Estupefacientes,Enfermedades Cronicas Degenerativas,Descripcion de Enfermedades Cronicas Degenerativas";
string dbV_D = "Placas,Tipo,Marca,Modelo,Rodado,Numero de Llantas,Transmicion,Cilindraje,Numero de Puertas,Asignado a";

#include"error.h"
#include"io.h"
#include"search.h"
#include"add.h"
#include"modify.h"
#include"delete.h"
#include"show.h"

bool main_menu()
{
    system("cls");
    cout<<"1.-Salir"<<endl
        <<"2.-Alta"<<endl
        <<"3.-Busqueda"<<endl
        <<"4.-Modificar"<<endl
        <<"5.-Eliminar"<<endl
        <<"6.-Mostrar Empleados entre 30 y 40"<<endl;
    int option;
    cin>>option;
    switch(option)
    {
        case 1:
            return false;
        break;
        case 2:
            alta();
            return true;
        case 3:
            busqueda();
            return true;
        break;
        case 4:
            modificar();
            return true;
        break;
        case 5:
            eliminar();
            return true;
        break;
        case 6:
            mostrar();
            return true;
        break;
        default:
            return true;
        break;
    }
}

int main()
{
    initial_check();
    read_data();
    while(main_menu()){};
}
