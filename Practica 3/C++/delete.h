/*
Eliminar
Status: Terminado
*/

void eliminarP(unsigned int clave) //Completo
{
    system("cls");
    for(int x=0;x<4;x++)
    {
        persona *actual = &baseP[x];
        persona *anterior = actual;
        while(actual->sgt!=NULL)
        {
            if(actual->clave==clave)
            {
                anterior->sgt = actual->sgt;
                delete(actual);
                cout<<"La persona con la clave "<<clave<<" ha sido eliminada de la base de datos"<<endl;
                write_data();
                return;
            }
            anterior = actual;
            actual = actual->sgt;
        }

    }
    cout<<"No existe el registro para la persona con la clave "<<clave<<endl;
}

void eliminarV(string placa) //Completo
{
    system("cls");
    for(int x=0;x<5;x++)
    {
        vehiculo *actual = &baseV[x];
        vehiculo *anterior = actual;

        while(actual->sgt != NULL)
        {
            if(actual->placas==placa)
            {
                anterior->sgt = actual->sgt;
                delete(actual);
                write_data();
                cout<<"El vehiculo con la placa "<<placa<<" ha sido eliminado de la base de datos"<<endl;
                return;
            }
            anterior = actual;
            actual = actual->sgt;
        }

    }
    cout<<"No existe el registro para el vehiculo con la placa "<<placa<<endl;
}

void eliminar() //Completo
{
    system("cls");
    string data;
    cout<<"Ingrese Clave/Placa: ";
    cin>>data;
    if(is_number(data))
    {
        eliminarP(atoi(data.c_str()));
        cin.get();
        cin.get();
    }else
    {
        eliminarV(data);
        cin.get();
        cin.get();
    }
}

