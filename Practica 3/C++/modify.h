/*
Modificar
Status: Terminado
*/

void modificarP(unsigned int clave)
{
    system("cls");
    int temp;
    if(!busquedaP(clave))
    {
        cin.get();
        cin.get();
        return;
    }
    cout<<"�Modificar este registro?"<<endl
        <<"1.-Si"<<endl
        <<"2.-No"<<endl;
    cin>>temp;
    if(temp!=1)
        return;
    system("cls");
    //Clave
    cout<<"Clave: ";
    tempP.clave = clave;
    cout<<tempP.clave<<endl;
    //Nombre
    cout<<"Nombre: ";
    cin>>tempP.nombre;
    //Apellido Paterno
    cout<<"Apellido Paterno: ";
    cin>>tempP.apellidoP;
    //Apellido Materno
    cout<<"Apellido Materno: ";
    cin>>tempP.apellidoM;
    //Fecha de Nacimiento
    string aux;
    int day,month,year;
    cout<<"Dia de Nacimiento (DD): ";
    cin>>day;
    cout<<"Mes de Nacimiento (MM): ";
    cin>>month;
    cout<<"A�o de Nacimiento (AAAA): ";
    cin>>year;
    ostringstream ss;
    ss<<day<<"\\"<<month<<"\\"<<year;
    aux = ss.str();
    strcpy(tempP.fechaNac,aux.c_str());
    //Edad
    time_t theTime = time(NULL);
    struct tm *aTime = localtime(&theTime);
    tempP.edad=0;
    if(month>aTime->tm_mon+1)
    {
        tempP.edad = aTime->tm_year + 1900- 1 - year ;
    }else
    {
        tempP.edad = aTime->tm_year + 1900 - year;
    }
    cout<<"Edad: "<<tempP.edad<<endl;
    //Hora de Alta
    ss.str("");
    ss<<aTime->tm_hour<<":"<<aTime->tm_min;
    aux=ss.str();
    strcpy(tempP.horaAlta,aux.c_str());
    cout<<"Hora de Alta: "<<tempP.horaAlta<<endl;
    //Curp
    cout<<"Curp: ";
    cin>>tempP.curp;
    //Rfc
    cout<<"RFC: ";
    cin>>tempP.rfc;
    //Tipo de Sangre
    cout<<"Tipo de Sangre: (O+):";
    cin>>tempP.sangre;
    //Padecimientos
    cout<<"Padecimientos: "<<endl
        <<"1.-Si"<<endl
        <<"2.-No"<<endl;
    cin>>temp;
    if(temp==1)
    {
        cout<<"Descripcion de padecimientos: ";
        cin.get();
        getline(cin,tempP.padecimientos);
    }else
    {
        tempP.padecimientos="No";
    }
    //Estupefacientes
    cout<<"Consume Estupefacientes: "<<endl
        <<"1.-Si"<<endl
        <<"2.-No"<<endl;
    cin>>temp;
    if(temp==1)
        strcpy(tempP.estupefacientes,"Si");
    else
        strcpy(tempP.estupefacientes,"No");
    //Enfermedades Cronicas
    cout<<"Tiene enfermedades Cronicas Degenerativas?"<<endl
        <<"1.-Si"<<endl
        <<"2.-No"<<endl;
    cin>>temp;
    cin.get();
    if(temp==1)
    {
        cout<<"Enfermedades Cronicas Degenerativas: ";
        getline(cin,tempP.enfCron);
        cout<<"Descripcion de Enfermedades Cronicas Degenerativas: ";
        getline(cin,tempP.enfCronD);
    }else
    {
        tempP.enfCron="No";
        tempP.enfCronD="No";
    }
    //Correccion
    cout<<"�Estos datos son correctos?"<<endl
        <<"1.-Si"<<endl
        <<"2.-No"<<endl;
    cin>>temp;
    if(temp==1)
    {
        //Escribir en archivo
        for(int x=0;x<4;x++)
        {
            persona *modificar = baseP[x].sgt;
            while(modificar->sgt != NULL)
            {
                if(modificar->clave==clave)
                {
                    modificar->clave = tempP.clave;
                    modificar->nombre = tempP.nombre;
                    modificar->apellidoP = tempP.apellidoP;
                    modificar->apellidoM = tempP.apellidoM;
                    strcpy(modificar->fechaNac,tempP.fechaNac);
                    strcpy(modificar->horaAlta,tempP.horaAlta);
                    strcpy(modificar->curp,tempP.curp);
                    strcpy(modificar->rfc,tempP.rfc);
                    strcpy(modificar->sangre,tempP.sangre);
                    modificar->padecimientos = tempP.padecimientos;
                    strcpy(modificar->estupefacientes,tempP.estupefacientes);
                    modificar->enfCron = tempP.enfCron;
                    modificar->enfCronD = tempP.enfCronD;
                    write_data();
                    return;
                }
            }
        }

    }
    return;
}

void modificarV(string placa)
{
    system("cls");
    int temp;
    if(!busquedaV(placa))
    {
        cin.get();
        cin.get();
        return;
    }
    cout<<"�Modificar este registro?"<<endl
        <<"1.-Si"<<endl
        <<"2.-No"<<endl;
    cin>>temp;
    if(temp!=1)
        return;
    system("cls");
    //Placas
    cout<<"Placas: ";
    cin>>tempV.placas ;
    //Tipo
    cout<<"Tipo: "<< tempV.tipo<<endl;
    //Marca
    cout<<"Marca: ";
    cin>>tempV.marca;
    //Modelo
    cout<<"Modelo (AAAA): ";
    cin>>tempV.modelo;
    //Rodado
    cout<<"Rodado: ";
    cin>>tempV.rodado;
    //Numero de Llantas
    cout<<"Numero de Llantas: ";
    cin>>tempV.llantas;
    //Cilindraje
    cout<<"Cilindraje: ";
    cin>>tempV.cilindraje;
    //Numero de Puertas
    cout<<"Numero de Puertas: ";
    cin>>tempV.puertas;
    //Asignado a
    cout<<"Asignado a: ";
    cin>>tempV.asignado;
    //Correccion
    cout<<"�Estos datos son correctos?"<<endl
        <<"1.-Si"<<endl
        <<"2.-No"<<endl;
    cin>>temp;
    if(temp==1)
    {
        //Escribir en archivo
        for(int x=0;x<4;x++)
        {
            vehiculo *modificar = baseV[x].sgt;
            while(modificar->sgt != NULL)
            {
                if(modificar->placas==placa)
                {
                    modificar->placas = tempV.placas;
                    modificar->tipo = tempV.tipo;
                    modificar->marca = tempV.marca;
                    modificar->modelo = tempV.modelo;
                    modificar->llantas = tempV.llantas;
                    modificar->transmicion = tempV.transmicion;
                    modificar->cilindraje= tempV.cilindraje;
                    modificar->puertas = tempV.puertas;
                    modificar->asignado = tempV.asignado;
                    write_data();
                    return;
                }
            }
        }

    }
    return;
}

void modificar()
{
    system("cls");
    string data;
    cout<<"Ingrese Clave/Placa: ";
    cin>>data;
    if(is_number(data))
    {
        modificarP(atoi(data.c_str()));
    }else
    {
        modificarV(data);
    }
}

