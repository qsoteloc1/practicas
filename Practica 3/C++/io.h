/*
Escritura y Lectura
Status: Terminado
*/

void read_data() //Completo
{
    fstream csv;
    //Base de Datos Personas
    for(int x=0;x<4;x++)
    {
        csv.open(dbP_N[x].c_str(),ios::in);
        string line,temp;
        persona *nueva = new(persona);
        baseP[x].sgt = nueva;
        getline(csv,line);
        while(getline(csv,line,'\n'))
        {
            //Guardar Clave
            temp = line.substr(0,line.find(','));
            nueva->clave = atoi(temp.c_str());
            line.erase(0,line.find(',')+1);
            //Guardar Nombre
            temp = line.substr(0,line.find(','));
            nueva->nombre = temp;
            line.erase(0,line.find(',')+1);
            //Guardar Apellido Paterno
            temp = line.substr(0,line.find(','));
            nueva->apellidoP = temp;
            line.erase(0,line.find(',')+1);
            //Guardar Apellido Materno
            temp = line.substr(0,line.find(','));
            nueva->apellidoM = temp;
            line.erase(0,line.find(',')+1);
            //Guardar Fecha Nacimiento
            temp = line.substr(0,line.find(','));
            strcpy(nueva->fechaNac,temp.c_str());
            line.erase(0,line.find(',')+1);
            //Guardar Edad
            temp = line.substr(0,line.find(','));
            nueva->edad = atoi(temp.c_str());
            line.erase(0,line.find(',')+1);
            //Guardar Hora de Alta
            temp = line.substr(0,line.find(','));
            strcpy(nueva->horaAlta,temp.c_str());
            line.erase(0,line.find(',')+1);
            //Guardar Curp
            temp = line.substr(0,line.find(','));
            strcpy(nueva->curp,temp.c_str());
            line.erase(0,line.find(',')+1);
            //Guardar rfc
            temp = line.substr(0,line.find(','));
            strcpy(nueva->rfc,temp.c_str());
            line.erase(0,line.find(',')+1);
            //Guardar Tipo de Sangre
            temp = line.substr(0,line.find(','));
            strcpy(nueva->sangre,temp.c_str());
            line.erase(0,line.find(',')+1);
            //Guardar Padecimientos
            temp = line.substr(0,line.find(','));
            nueva->padecimientos = temp;
            line.erase(0,line.find(',')+1);
            //Guardar Estupefacientes
            temp = line.substr(0,line.find(','));
            strcpy(nueva->estupefacientes,temp.c_str());
            line.erase(0,line.find(',')+1);
            //Guardar Enfermedades Cronicas Degenerativas
            temp = line.substr(0,line.find(','));
            nueva->enfCron = temp;
            line.erase(0,line.find(',')+1);
            //Guardar Descripcion de Enfermedades Cronicas Degenerativas
            temp = line.substr(0,line.find(','));
            nueva->enfCronD = temp;
            line.erase(0,line.find(',')+1);

            persona *siguiente = new(persona);
            nueva->sgt = siguiente;
            nueva = siguiente;
        }
        csv.close();
    }
    //Base de Datos Vehiculos
    for(int x=0;x<5;x++)
    {
        csv.open(dbV_N[x].c_str(),ios::in);
        string line,temp;
        vehiculo *nuevo = new(vehiculo);
        baseV[x].sgt = nuevo;
        getline(csv,line);
        while(getline(csv,line,'\n'))
        {
            //Guardar Placas
            temp = line.substr(0,line.find(','));
            nuevo->placas = temp;
            line.erase(0,line.find(',')+1);
            //Guardar Tipo
            temp = line.substr(0,line.find(','));
            nuevo->tipo = temp;
            line.erase(0,line.find(',')+1);
            //Guardar Marca
            temp = line.substr(0,line.find(','));
            nuevo->marca = temp;
            line.erase(0,line.find(',')+1);
            //Guardar Modelo
            temp = line.substr(0,line.find(','));
            nuevo->modelo = atoi(temp.c_str());
            line.erase(0,line.find(',')+1);
            //Guardar Rodado
            temp = line.substr(0,line.find(','));
            nuevo->rodado = atoi(temp.c_str());
            line.erase(0,line.find(',')+1);
            //Guardar Numero de Llantas
            temp = line.substr(0,line.find(','));
            nuevo->llantas = atoi(temp.c_str());
            line.erase(0,line.find(',')+1);
            //Guardar Transmicion
            temp = line.substr(0,line.find(','));
            nuevo->transmicion = temp;
            line.erase(0,line.find(',')+1);
            //Guardar Cilindraje
            temp = line.substr(0,line.find(','));
            nuevo->cilindraje = atoi(temp.c_str());
            line.erase(0,line.find(',')+1);
            //Guardar Numero de puertas
            temp = line.substr(0,line.find(','));
            nuevo->puertas = atoi(temp.c_str());
            line.erase(0,line.find(',')+1);
            //Guardar Asignado a
            temp = line.substr(0,line.find(','));
            nuevo->asignado = temp;
            line.erase(0,line.find(',')+1);

            vehiculo *siguiente = new(vehiculo);
            nuevo->sgt = siguiente;
            nuevo = siguiente;
        }
        csv.close();
    }
}

void write_data() //Completo
{
    fstream csv;
    //Base de Datos Personas
    for(int x=0;x<4;x++)
    {
        csv.open(dbP_N[x].c_str(),ios::out|ios::trunc);
        persona *siguiente = baseP[x].sgt;

        csv<<dbP_D<<endl;
        while(siguiente->sgt != NULL)
        {
            csv <<siguiente->clave<<","
            <<siguiente->nombre<<","
            <<siguiente->apellidoP<<","
            <<siguiente->apellidoM<<","
            <<siguiente->fechaNac<<","
            <<siguiente->edad<<","
            <<siguiente->horaAlta<<","
            <<siguiente->curp<<","
            <<siguiente->rfc<<","
            <<siguiente->sangre<<","
            <<siguiente->padecimientos<<","
            <<siguiente->estupefacientes<<","
            <<siguiente->enfCron<<","
            <<siguiente->enfCronD<<endl;
            siguiente=siguiente->sgt;
        }
        csv.close();
    }
    //Base de Datos Vehiculos

    for(int x=0;x<5;x++)
    {
        csv.open(dbV_N[x].c_str(),ios::out|ios::trunc);
        csv<<dbV_D<<endl;
        vehiculo *siguiente = baseV[x].sgt;
        while(siguiente->sgt != NULL)
        {
            csv <<siguiente->placas<<","
                <<siguiente->tipo<<","
                <<siguiente->marca<<","
                <<siguiente->modelo<<","
                <<siguiente->rodado<<","
                <<siguiente->llantas<<","
                <<siguiente->transmicion<<","
                <<siguiente->cilindraje<<","
                <<siguiente->puertas<<","
                <<siguiente->asignado<<endl;
            siguiente=siguiente->sgt;
        }
        csv.close();
    }
}

