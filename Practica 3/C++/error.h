/*
Errores
Status: Funcional
*/

#define err_code1 1     //Bases de Datos "Personas" Corrompidas
#define err_code2 2     //Bases de Datos "Vehiculos" Corrompidas

void initial_check() //Completo
{
    system("cls");
    fstream csv;
    //Base de Datos Personas
    for(int x=0;x<4;x++)
    {
        csv.open(dbP_N[x].c_str(),ios::in);
        if(!csv.is_open())
        {
            csv.open(dbP_N[x].c_str(),ios::out|ios::trunc);
            csv<<dbP_D<<endl;
        }else
        {
            string temp;
            getline(csv,temp,'\n');
            if(temp!=dbP_D)
                exit(err_code1);
        }
        csv.close();
    }
    //Base de Datos Vehiculos
    for(int x=0;x<5;x++)
    {
        csv.open(dbV_N[x].c_str(),ios::in);
        if(!csv.is_open())
        {
            csv.open(dbV_N[x].c_str(),ios::out|ios::trunc);
            csv<<dbV_D<<endl;
        }else
        {
            string temp;
            getline(csv,temp,'\n');
            if(temp!=dbV_D)
                exit(err_code1);
        }
        csv.close();
    }
}
