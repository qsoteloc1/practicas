import csv
import os


class Archive:
    def __init__(self):
        self.data = []
        self.read_data()

    def read_data(self):
        self.data = []
        with open("prac5-valores.csv") as database:
            line = csv.reader(database, delimiter=',')
            for row in line:
                self.data.append(row)

    def write_data(self):
        with open("prac5-valores.csv",mode='w',newline='\n') as database:
            line = csv.writer(database, delimiter=',')
            for row in self.data:
                line.writerow(row)
        self.read_data()

    def get(self, bus_id):
        print(self.data[bus_id])
        input("Presione enter para continuar")

    def edit(self, bus_id, cell, value):
        self.data[bus_id][cell] = value


class Menu:
    def __init__(self):
        self.menu_op = 0

    def show_menu(self):
        os.system('cls')
        print("Logistic Beach")
        print("Software de Evaluacion")
        print("1.-Modificar un solo viaje de un Camion")
        print("2.-Modificar todos los viajes de un Camion")
        print("3.-Buscar un camion")
        print("4.-Obtener promedios")
        print("5.-Salir")

    def selection(self):
        self.menu_op = int(input("Opcion: "))
        if self.menu_op == 1:
            mod_trip_s()
        elif self.menu_op == 2:
            mod_trip_l()
        elif self.menu_op == 3:
            search_bus()
        elif self.menu_op == 4:
            get_averange()



def mod_data_s(bus_id):
    cell = int(input("Viaje a modificar: "))
    if(cell>0 and cell<=20):
        value = input("Nuevo Valor: ")
        if int(value)>=0 and int(value)<=10:
            database.edit(bus_id, cell+1, value)
            database.write_data()
        else:
            print("Valor fuera de rango")
            input("Presione enter para continuar")
            return
    else:
        print("Viaje Inexistente")
        input("Presione enter para continuar")


def mod_data_l(bus_id):
    for x in range(20):
        print("Modificando viaje "+str(x+1)+": ")
        value = input("Nuevo Valor: ")
        if int(value)>=0 and int(value)<=10:
            database.edit(bus_id, x+2, value)
        else:
            print("Valor fuera de rango")
            input("Presione enter para continuar")
            return

    database.write_data()


def mod_trip_s():
    bus_id = input("Id del camion a modificar: ")
    x = 0
    for row in database.data:
        if str(bus_id) == str(row[0]):
            mod_data_s(x)
            return
        else:
            x += 1
    print("El camion con el ID: " + str(bus_id) + " no existe")
    input("Presione enter para continuar")

def mod_trip_l():
    bus_id = input("Id del camion a modificar: ")
    x = 0
    for row in database.data:
        if str(bus_id) == str(row[0]):
            mod_data_l(x)
            return
        else:
            x += 1
    print("El camion con el ID: " + str(bus_id) + " no existe")
    input("Presione enter para continuar")

def search_bus():
    bus_id = input("Id del camion a buscar: ")
    x = 0
    for row in database.data:
        if str(bus_id) == str(row[0]):
            database.get(x)
            return
        else:
            x += 1
    print("El camion con el ID: "+str(bus_id)+ " no existe")
    input("Presione enter para continuar")


def get_averange():
    y = 0
    file = open("promedio-camion.csv",'w+')
    file.write("Id,Nombre,Promedio\n")
    for row in database.data:
        counter = 0
        if(y==0):
            y = 1
        else:
            for x in range(20):
                counter += int(row[x+2])
            counter /= 20
            file.write(str(database.data[y][0])+","+str(database.data[y][1])+","+str(counter)+'\n')
            y += 1


main_menu = Menu()
database = Archive()
while main_menu.menu_op != 5:
    main_menu.show_menu()
    main_menu.selection()
