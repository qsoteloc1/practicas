/*
----------------------------------
Christian Orlando Quintanar Sotelo
           Practica 2
           10/11/2018
----------------------------------
*/

#include<iostream>
#include<fstream>
#include<windows.h>
#include<string>
#include<stdlib.h>


using namespace std;

string getfile() //Funcion para obtener el nombre del archivo a leer
{
    string temp="";
    cout<<"Archivos csv en carpeta: "<<endl<<endl;
    system("dir /b *.csv"); //Mostrar archivos en carpeta
    cout<<endl<<"Abrir: ";
    cin>>temp;
    if(temp.find('.')==string::npos){ //Verificar que contenga extension
        temp.append(".csv");
    }else{
        if(temp.substr(temp.find('.'))!=".csv"){
            system("cls");
            cout<<"El archivo seleccionado no es de tipo csv"<<endl;
            return "";
        }
    }
    ifstream csv;
    csv.open(temp.c_str()); //Verificar la existencia del archivo
    if(csv.is_open()){
        system("cls");
        csv.close();
        return temp;
    }else{
        system("cls");
        cout<<"El archivo \""<<temp<<"\" no existe"<<endl;
        return "";
    }
}

int file_parser(int (&station)[10000][10], int line_number ,string line) //Funcion para ingresar los datos al arreglo
{
    string temp;
    for(int x=0;x<9;x++)
    {
        temp = line.substr(0,line.find(','));
        station[line_number][x]=atoi(temp.c_str());
        line.erase(0,line.find(',')+1);
    }
    temp = line.substr(0,line.find('\n'));
    station[line_number][9]=atoi(temp.c_str());;
    return 0;
}

int main()
{
    int station[10000][10], line_number=0;
    string archive=getfile(), line;

    while(archive=="") //Ejecucion hasta seleccionar un archivo
    {
        archive=getfile();
    }

    fstream csv(archive.c_str()); //Abrir archivo
    if(csv.is_open()){
        getline(csv,line);
        system("cls");
        while(getline(csv,line)) //Leer y guardar datos
        {
            file_parser(station, line_number, line);
            line_number++;
            //cout<<"Se han leido: "<<line_number<<" lineas"<<endl;    <-Opcional mostrar lineas leidas durante ejecucion
        }
    }else{
        cout<<"Ha ocurrido un problema al intentar abrir el archivo \""<<archive<<"\" "<<endl;
        cout<<"Codigo de error 100"<<endl;
        system("Pause");
        return 100; //Codigo de error 100, problema al abrir un archivo
    }
    system("cls");
    csv.close(); //Cerrar Archivo
    archive= "resultados_" + archive;
    csv.open(archive.c_str(), std::ofstream::out | std::ofstream::trunc);
    if(!csv.is_open())
    {
        cout<<"Ha ocurrido un problema al intentar crear el archivo \""<<archive<<"\" "<<endl;
        cout<<"Codigo de error 101"<<endl;
        system("Pause");
        return 101;
    }

    for(int i=0;i<10000;i++) //Comparacion y escritura de datos procesados
    {
        csv<<"Producto "<<i+1<<endl;
        for(int x=0;x<9;x++){
            csv<<"F"<<x+1<<",";
        }
        csv<<"F"<<10<<endl;
        for(int j=0;j<9;j++)
        {
            if(station[i][j]>=0 && station[i][j]<=200)
            {
                csv<<"FMR"<<",";
            }
            else if(station[i][j]>=201 && station[i][j]<=500)
            {
                csv<<"FM"<<",";
            }
            else if(station[i][j]>=501 && station[i][j]<=799)
            {
                csv<<"FA"<<",";
            }
            else if(station[i][j]>=800 && station[i][j]<=1000)
            {
                csv<<"FE"<<",";
            }
        }
        if(station[i][9]>=0 && station[i][9]<=200)
            {
                csv<<"FMR"<<endl;
            }
            else if(station[i][9]>=201 && station[i][9]<=500)
            {
                csv<<"FM"<<endl;
            }
            else if(station[i][9]>=501 && station[i][9]<=799)
            {
                csv<<"FA"<<endl;
            }
            else if(station[i][9]>=800 && station[i][9]<=1000)
            {
                csv<<"FE"<<endl;
            }
    }
    csv.close();
    cout<<"Proceso completado con exito"<<endl;
    system(archive.c_str());

    return 0;

}
