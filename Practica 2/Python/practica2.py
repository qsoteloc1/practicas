import csv
import os
import numpy

def getfile():
    print('Archivos en directorio:\n')
    os.system("dir /b *.csv")
    filename=input('\nAbrir: ')
    if filename.find('.')==-1:
        filename+='.csv'
    return filename


values = numpy.zeros((10000, 10))
filename = getfile()
while not os.path.isfile(filename) or not filename[filename.find('.'):len(filename)]==".csv":
    if(not os.path.isfile(filename)):
        print("El archivo "+filename+" no existe")
    if(not filename[filename.find('.'):len(filename)]==".csv"):
        print("El archivo " + filename + " no es de tipo csv")
    filename = getfile()

if not open(filename):
    print("Ocurrio un problema al abrir el archivo")
    print("Codigo de error 100")
    exit(100)

with open(filename, newline='') as csv_file:
    reader = csv.reader(csv_file, delimiter=',')
    line = 0
    for row in reader:
        if line != 0:
            for x in range(10):
                values[line-1][x] = int(row[x])
            line += 1
        else:
            line += 1

filename = "resultados_"+filename

if not open(filename, 'w+'):
    print("Ocurrio un problema al crear el archivo")
    print("Codigo de error 101")
    exit(101)


with open(filename, 'w+', newline='') as csv_file:
    writer = csv.writer(csv_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    for x in range(10000):
        strhelp=""
        for i in range(9):
            if values[x][i]>=0 and values[x][i]<=200:
                strhelp+='FMR'+','
            elif values[x][i]>=201 and values[x][i]<=500:
                strhelp += 'FM' + ','
            elif values[x][i] >= 501 and values[x][i] <= 799:
                strhelp += 'FA' + ','
            elif values[x][i] >= 800 and values[x][i] <= 1000:
                strhelp += 'FE' + ','

        if values[x][9] >= 0 and values[x][9] <= 200:
            strhelp += 'FMR'
        elif values[x][9] >= 201 and values[x][9] <= 500:
            strhelp += 'FM'
        elif values[x][9] >= 501 and values[x][9] <= 799:
            strhelp += 'FA'
        elif values[x][9] >= 800 and values[x][9] <= 1000:
            strhelp += 'FE'
        strhelp = strhelp.split(',')
        writer.writerow(["Producto "+str(x+1)])
        writer.writerow(["F1","F2","F3","F4","F5","F6","F7","F8","F9","F10"])
        writer.writerow(strhelp)

print("Proceso completado con exito")
os.system(filename)